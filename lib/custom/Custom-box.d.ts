/// <reference types="react" />
interface Data {
    width: number;
    height: number;
    bgColor: string;
    content: string;
    color: string;
}
declare const CustomBox: ({ width, height, bgColor, content, color }: Data) => JSX.Element;
export default CustomBox;
