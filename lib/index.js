"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CustomBox = exports.HelloFC = void 0;
const Hello_fc_1 = __importDefault(require("./Hello-fc"));
exports.HelloFC = Hello_fc_1.default;
const Custom_box_1 = __importDefault(require("./custom/Custom-box"));
exports.CustomBox = Custom_box_1.default;
