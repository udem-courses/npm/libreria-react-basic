/**Function componet */
/// <reference types="react" />
interface Data {
    name: string;
}
declare const HelloFC: ({ name }: Data) => JSX.Element;
export default HelloFC;
