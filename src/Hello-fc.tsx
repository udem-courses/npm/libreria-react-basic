/**Function componet */

import React from 'react'

interface Data {
    name: string
}

const HelloFC = ({ name }: Data): JSX.Element => {
    if (!name) {
        name = "mundo"
    }
    return (
        <h1>Hola {name}</h1>
    )
}

export default HelloFC
