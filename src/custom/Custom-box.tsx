import React from 'react'
interface Data {
    width: number
    height: number
    bgColor: string
    content: string
    color: string
}
const CustomBox = ({ width, height, bgColor, content, color }: Data): JSX.Element => {
    const text: string = (!content) ? 'Texto de ejemplo' : content
    return (
        <div style={{
            width: width || 200,
            height: height || 200,
            backgroundColor: bgColor || "green",
            color: color || "white"
        }}>
            {text}
        </div>
    )
}

export default CustomBox
